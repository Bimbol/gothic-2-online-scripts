enum Color
{
	RED,
	GREEN,
	BLUE
};

local drawPos = null;
local currColor = Color.RED;
local colorValue = 150;

local function init()
{
	setCursorVisible(true);
	
	drawPos = drawCreatePx(0, 0, 150, 0, 0, "FONT_OLD_10_WHITE_HI.TGA", "");
	drawSetVisible(drawPos, true);
}

addEventHandler("onInit", init);

local function mouseClick(btn)
{
	colorValue = 150;
	
	switch (btn)
	{
	case MOUSE_LMB:
		currColor = Color.RED;
		drawSetColor(drawPos, colorValue, 0, 0);
		break;
		
	case MOUSE_RMB:
		currColor = Color.GREEN;
		drawSetColor(drawPos, 0, colorValue, 0);
		break;
		
	case MOUSE_MMB:
		currColor = Color.BLUE;
		drawSetColor(drawPos, 0, 0, colorValue);
		break;
	}
}

addEventHandler("onMouseClick", mouseClick);

local function mouseRelease(btn)
{
	local pos = getCursorPositionPx();
	
	drawSetText(drawPos, format("%d, %d", pos.x, pos.y));
	drawSetPositionPx(drawPos, pos.x, pos.y)
}

addEventHandler("onMouseRelease", mouseRelease);

local function mouseWheel(value)
{
	colorValue += (value * 5);

	switch (currColor)
	{
	case Color.RED:
		drawSetColor(drawPos, colorValue, 0, 0);
		break;
		
	case Color.GREEN:
		drawSetColor(drawPos, 0, colorValue, 0);
		break;
		
	case Color.BLUE:
		drawSetColor(drawPos, 0, 0, colorValue);
		break;
	}
}

addEventHandler("onMouseWheel", mouseWheel);